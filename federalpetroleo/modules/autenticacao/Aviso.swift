//
//  Aviso.swift
//  federalpetroleo
//
//  Created by Gustavo Cosme on 12/08/20.
//  Copyright © 2020 Safari. All rights reserved.
//

import UIKit

class Aviso: UIViewController {

    @IBOutlet weak var txtAviso: UILabel!
    @IBOutlet weak var icon: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()

        
        
        
    }
    
    @IBAction func onClickOK(sender: Any) {
        
        //Pref.add(params: "true", key: "logado")
        //self.present(AppDelegate.STAGE.getNavMain(), animated: true, completion: nil);
        
        self.present(AppDelegate.STAGE.getNavAutenticacao(), animated: true, completion: nil);
        
     }
    
    
     @IBAction func onClickClose(sender: Any) {
        
        dismiss(animated: true, completion: nil);
        
     }
    
    
    
    
    

}
