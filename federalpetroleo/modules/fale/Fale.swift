//
//  Fale.swift
//  federalpetroleo
//
//  Created by Gustavo Cosme on 13/06/20.
//  Copyright © 2020 Safari. All rights reserved.
//

import UIKit
import MessageUI

class Fale: UIViewController, MFMailComposeViewControllerDelegate {
    
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtTelefone: UITextField!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtEmail.text = DataUser.CURRENT.postoAtual["email"].string!;
        txtTelefone.text = DataUser.CURRENT.postoAtual["telefone"].string!;

    }
    
    @IBAction func onClickEmail(_ e:Any)
    {
        
        let emailTitle = "Fale com Comercial"
        let messageBody = ""
        let toRecipents = [DataUser.CURRENT.postoAtual["email"].string!]
        let mc: MFMailComposeViewController = MFMailComposeViewController()
        mc.mailComposeDelegate = self
        mc.setSubject(emailTitle)
        mc.setMessageBody(messageBody, isHTML: false)
        mc.setToRecipients(toRecipents)
        self.present(mc, animated: true, completion: nil)
        
        
    }
    
    @IBAction func onClickPhone(_ e:Any)
    {
        
        guard let url = URL(string: "tel://+55\(DataUser.CURRENT.postoAtual["telefone"].string!)") else {
            return
        }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url)
        } else {
            UIApplication.shared.openURL(url)
        }
        
    }
    
    @IBAction func onClickZap(_ any:Any)
    {
        
        
        //let urlWhats = "whatsapp://send?phone=+5581971037502";
        
        let urlWhats = "https://wa.me/55\(DataUser.CURRENT.postoAtual["telefone"].string!)";
        
        if let urlString = urlWhats.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) {
            if let whatsappURL = URL(string: urlString) {
                if UIApplication.shared.canOpenURL(whatsappURL) {
                    UIApplication.shared.openURL(whatsappURL)
                } else {
                    
                    print("INSTALE O ZAP");
                    
                }
            }
        }
        
        
        
    }
    
    
    
    @IBAction func onClickClose(_ e:Any)
    {
        dismiss(animated: true, completion: nil);
    }
    
    
}
