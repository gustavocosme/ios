//
//  PetsCell.swift
//  zoapet
//
//  Created by Gustavo Cosme on 08/02/17.
//  Copyright © 2017 SafariStudio. All rights reserved.
//

import UIKit
import SwiftyJSON

extension String {
    var doubleValue:Double? {
        return (self as NSString).doubleValue
    }
}

class CotacaoCell: UITableViewCell,UITextFieldDelegate {
    
    
    
    @IBOutlet weak var titulo              :UILabel!;
    //@IBOutlet weak var valor               :UILabel!;
    @IBOutlet weak var dias                :UILabel!;
    @IBOutlet weak var litros               :UITextField!;
    
    var data:DataCotacao!;
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        print("textFieldDidChange")
        if let i = litros.text!.doubleValue {
            Cotacao.INSTANCE.onUpdateLitros(posicao: self.data.posicao, litros: i, listrosStr:litros.text!.replace(target: ".", withString:""))
        } else{
            Cotacao.INSTANCE.addAlertOK(value:"Inválido");
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
         /*if let amountString = textField.text?.currencyInputFormatting() {
               textField.text = amountString
         Cotacao.INSTANCE.onUpdateLitros(posicao: self.data.posicao, litros: 0.0, listrosStr: amountString)
        }*/
        return true
    }
   
    func inicializa(data:DataCotacao){
        litros.keyboardType = .decimalPad
        self.data = data;
        titulo.text = data.titulo;
        litros.text = data.litrosStr;
        litros.addTarget(self, action: #selector(CotacaoCell.textFieldDidChange(_:)),
                                  for: .editingChanged)
        dias.text = data.diaStr;
    }
    
    @IBAction func onClickDias(_ value: Any)
    {
        
        DispatchQueue.main.async {

            
                let pickerData = [
                       ["value": "1 dia", "display": "1 dia"],
                       ["value": "2 dias", "display": "2 dias"],
                       ["value": "3 dias", "display": "3 dias"],
                       ["value": "4 dias", "display": "4 dias"],
                       ["value": "5 dias", "display": "5 dias"],
                       ["value": "6 dias", "display": "6 dias"],
                       ["value": "7 dias", "display": "7 dias"],
                       ["value": "8 dias", "display": "8 dias"],
                       ["value": "9 dias", "display": "9 dias"],
                       ["value": "10 dias", "display": "10 dias"],
                       ["value": "11 dias", "display": "11 dias"],
                       ["value": "12 dias", "display": "12 dias"],
                       ["value": "13 dias", "display": "13 dias"],
                       ["value": "14 dias", "display": "14 dias"]

                   ]

            PickerDialog().show(title: "Dias", options: pickerData, selected: "0") {
                       (value) -> Void in

                let v = Int(value.split(split: " ")[0]) ?? -1;
                Cotacao.INSTANCE.onUpdateDias(posicao: self.data.posicao,dias:value,posicaoDia:v-1)

            }
                
        }
    
        
        
        /*
        
           let actionSheetController: UIAlertController = UIAlertController(title:nil, message: "Dias", preferredStyle: .alert);

           let names:[String] = ["1 Dia","2 Dias","3 Dias","4 Dias","5 Dias","6 Dias","7 Dias","8 Dias","9 Dias","10 Dias"," 11 Dias","12 Dias","13 Dias","14 Dias"];
                 
                 
             for name in names {
               
                 let item: UIAlertAction = UIAlertAction(title: name, style: .default) { action -> Void in
                     
                    Cotacao.INSTANCE.onUpdateDias(posicao: self.data.posicao,dias:name)
                    
                 }
               
                 actionSheetController.addAction(item)

             }
             
            Cotacao.INSTANCE.present(actionSheetController, animated: true, completion: nil)
        */
        
        /*
        
        let alert = UIAlertController(style: .actionSheet, title: "Selecione", message: "Dias")

        let frameSizes: [CGFloat] = (1...15).map { CGFloat($0) }
        let pickerViewValues: [[String]] = [frameSizes.map { Int($0).description }]
        let pickerViewSelectedValue: PickerViewViewController.Index = (column: 0, row: frameSizes.index(of: 216) ?? 0)

        alert.addPickerView(values: pickerViewValues, initialSelection: pickerViewSelectedValue) { vc, picker, index, values in
            DispatchQueue.main.async {
                UIView.animate(withDuration: 1) {
                    
                    var d = "dias"
                    
                    if index.row == 0
                    {
                        d = "dia";
                    }
                    
                    Cotacao.INSTANCE.onUpdateDias(posicao: self.data.posicao,dias:"\(index.row+1) \(d)")

                    vc.preferredContentSize.height = frameSizes[index.row]
                }
            }
        }
        alert.addAction(title: "Cancelar", style: .cancel)
        //
        
        DispatchQueue.main.async {

            Cotacao.INSTANCE.present(alert, animated: true, completion: nil)

        }
        */

           
       }
    
    @IBAction func onClickListros(_ a: Any)
    {
        
        DispatchQueue.main.async {

            
                let pickerData = [
                       ["value": "2.000", "display": "2.000"],
                       ["value": "2.500", "display": "2.500"],
                       ["value": "3.000", "display": "3.000"],
                       ["value": "4.000", "display": "4.000"],
                       ["value": "5.000", "display": "5.000"],
                       ["value": "10.000", "display": "10.000"],
                       ["value": "13.000", "display": "13.000"],
                       ["value": "23.000", "display": "23.000"],
                       ["value": "25.000", "display": "25.000"],
                   ]

            PickerDialog().show(title: "Litros", options: pickerData, selected: "0") {
                       (value) -> Void in

                self.litros.text = value
                
                if let i = self.litros.text!.doubleValue {
                    Cotacao.INSTANCE.onUpdateLitros(posicao: self.data.posicao, litros: i, listrosStr:self.litros.text!.replace(target: ".", withString:""))
                } else{
                    Cotacao.INSTANCE.addAlertOK(value:"Inválido");
                }
                

            }
                
        }
    
        
        
    
           
       }
    
    
    
    
 
    
}//END CLASS

extension String {

    // formatting text for currency textField
    func currencyInputFormatting() -> String {

        var number: NSNumber!
        let formatter = NumberFormatter()
        formatter.numberStyle = .currencyAccounting
        formatter.currencySymbol = ""
        formatter.maximumFractionDigits = 2
        formatter.minimumFractionDigits = 2

        var amountWithPrefix = self

        // remove from String: "$", ".", ","
        let regex = try! NSRegularExpression(pattern: "[^0-9]", options: .caseInsensitive)
        amountWithPrefix = regex.stringByReplacingMatches(in: amountWithPrefix, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, self.count), withTemplate: "")

        let double = (amountWithPrefix as NSString).doubleValue
        number = NSNumber(value: (double / 100))

        // if first number is 0 or all numbers were deleted
        guard number != 0 as NSNumber else {
            return ""
        }

        return formatter.string(from: number)!
    }
}

extension Double {
        
       func roundToDecimal(_ fractionDigits: Int) -> Double {
           let multiplier = pow(10, Double(fractionDigits))
           return Darwin.round(self * multiplier) / multiplier
       }
}
