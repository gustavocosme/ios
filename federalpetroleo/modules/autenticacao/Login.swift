//
//  LoginViewController.swift
//  vendaseuingresso
//
//  Created by Gustavo Cosme on 29/08/17.
//  Copyright © 2017 Safari. All rights reserved.
//

import UIKit
import PKHUD
import SwiftyJSON;
import OneSignal;

class Login: UIViewController, UITextFieldDelegate {
    
    //###################################################//
    //MARK:MODEL
    //###################################################//
    
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtSenha: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad();
        
     
        
                
        txtEmail.delegate = self;
        txtSenha.delegate = self;
        
        txtEmail.tag = 0;
        txtSenha.tag = 1;
        
        if(AppDelegate.IS_TESTE)
        {
            //jorge@safari.com.br
            //123123
            txtEmail.text = "gustavo@safari.com.br";
            txtSenha.text = "1111";
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    @IBAction func onClickClose(sender: Any) {
        
        dismiss(animated: true, completion: nil);
        
    }
    
    @IBAction func onClickEsqueceu(sender: Any) {
        
        
    }
    
    
    @IBAction func onClickLogin(_ sender: Any) {
        
        self.initModel();
        
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if textField.tag < 2 {
            
        } else {
            textField.resignFirstResponder()
            initModel();
        }
        
        return false
    }
    
    
    func initModel()
    {
  
        if(!isOk())
        {
            return;
        }
        
        var p:[String:Any] = [String:Any]();
        p["password"] = txtSenha.text!;
        p["email"] = txtEmail.text!;
        p["profile_id"] = "2"
        
        let userId = OneSignal.getPermissionSubscriptionState().subscriptionStatus.userId ?? "-1"
        p["push"] = userId;
        
        print("PP: "+userId)

        HTTP.request(URL:"\(HTTP.SERVER)user/login",params:p,isLoad:true,completion:{json,status in
         
            
            if(json["status"].bool!)
            {
                if(json["data"]["usuario"]["liberado"].int! == 2)
                {
                    
                    if(json["data"]["usuario"]["primeiro_acesso"].int! == 0)
                    {
                        DataUser.tokenAux = json["data"]["token"].string!;
                        self.present(AppDelegate.STAGE.getSenhas(), animated: true, completion: nil);
                    }else{
                        
                        DataUser.save(json: json,token:json["data"]["token"].string!);
                        self.present(AppDelegate.STAGE.getMain(), animated: true, completion: nil);

                    }
                    
                }else{
                    
                    self.present(AppDelegate.STAGE.getAviso(), animated: true, completion: nil);

                }
            
            }
            else{
                
                self.addAlertOK(value: "Login inválido!")
                
            }
         
                      
            
        });
        
        
        
     
        
    }
    
    func isOk()->Bool
    {
        

        if(txtEmail.text!.count <= 0)
        {
            addAlertOK(value: "E-mail inválido");
            return false;
        }
        
        if(!FormsUtils.isValidEmail(testStr: txtEmail.text!))
        {
            addAlertOK(value: "E-mail inválido");
            return false;
        }
        
        
        if(txtSenha.text!.count <= 0)
        {
            addAlertOK(value: "Senha inválida");
            return false;
        }
        
        return true;
        
    }
    
    
    
}
