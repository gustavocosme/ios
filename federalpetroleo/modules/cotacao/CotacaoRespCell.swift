//
//  PetsCell.swift
//  zoapet
//
//  Created by Gustavo Cosme on 08/02/17.
//  Copyright © 2017 SafariStudio. All rights reserved.
//

import UIKit
import SwiftyJSON

class CotacaoRespCell: UITableViewCell {
    
    
    
    @IBOutlet weak var titulo              :UILabel!;
    @IBOutlet weak var dias                :UILabel!;
    @IBOutlet weak var litros              :UILabel!;
    @IBOutlet weak var quantidade          :UILabel!;
    @IBOutlet weak var total          :UILabel!;

    var data:DataCotacao!;
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
   
    func inicializa(data:DataCotacao)
    {

        self.data           = data;
        titulo.text         = data.titulo;
        dias.text           = data.diaStr;
        quantidade.text     = data.litrosStr+"L";
        litros.text         = "\(data.valor)";
        
        let a = data.valor.replace(target: "R$ ", withString:"").replace(target: ",", withString: ".").doubleValue;
        //print(a);
        let b = data.litros * a!;
        
        let currencyFormatter = NumberFormatter()
        currencyFormatter.usesGroupingSeparator = true
        currencyFormatter.numberStyle = .currency
        currencyFormatter.locale = Locale(identifier: "pt_BR")
        let priceString = currencyFormatter.string(from: NSNumber(value: b))!
        total.text          = "\(priceString)";

    }
    
  
    
    
    
    
 
    
}//END CLASS
