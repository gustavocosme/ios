//
//  LoginViewController.swift
//  vendaseuingresso
//
//  Created by Gustavo Cosme on 29/08/17.
//  Copyright © 2017 Safari. All rights reserved.
//

import UIKit
import PKHUD
import SwiftyJSON;
import OneSignal;
import JMMaskTextField_Swift;


class Cadastro: UIViewController, UITextFieldDelegate {
    
    //###################################################//
    //MARK:MODEL
    //###################################################//
    
    //@IBOutlet weak var txtNome: UITextField!
    @IBOutlet weak var txtRazao: JMMaskTextField!
    @IBOutlet weak var txtCpf: JMMaskTextField!
    @IBOutlet weak var txtSenha: UITextField!
    @IBOutlet weak var txtConfirmarSenha: UITextField!
    @IBOutlet weak var txtTelefone: JMMaskTextField!
    @IBOutlet weak var txtCnpj: JMMaskTextField!
    @IBOutlet weak var txtEstado: UITextField!
    @IBOutlet weak var txtEndereco: UITextField!
    
    @IBOutlet weak var txtNome: UITextField!
    @IBOutlet weak var txtEmail: UITextField!

    
    //@IBOutlet weak var txtCep: UITextField!
    //@IBOutlet weak var txtBairro: UITextField!
    //@IBOutlet weak var txtCidade: UITextField!
    //@IBOutlet weak var txtComplemento: UITextField!

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        txtNome.delegate = self;
        txtRazao.delegate = self;
        txtCpf.delegate = self;
        txtSenha.delegate = self;
        txtConfirmarSenha.delegate = self;
        txtTelefone.delegate = self;
        txtCnpj.delegate = self;
        txtEstado.delegate = self;
        txtEndereco.delegate = self;
        
        txtNome.tag = 1;
        txtRazao.tag = 2;
        txtCpf.tag = 3;
        txtSenha.tag = 4;
        txtConfirmarSenha.tag = 5;
        txtTelefone.tag = 6;
        txtCnpj.tag = 7;
        txtEstado.tag = 8;
        txtEndereco.tag = 9;
        
        txtCpf.maskString = "000.000.000-00"
        txtTelefone.maskString = "(00)00000-0000"
        txtCnpj.maskString = "00.000.000/0000-00"
        //txtRazao.maskString = "00.000.000/0000-00"

        if(AppDelegate.IS_TESTE)
        {
            let maskCPF = JMStringMask(mask: "000.000.000-00")
            let maskedStringCPF = maskCPF.mask(string:"371.245.920-38");
            
            let maskCNPJ = JMStringMask(mask: "00.000.000/0000-00")
            let maskedStringCNPJ = maskCNPJ.mask(string:"60.371.736/0001-06");
            
            let maskTelefone = JMStringMask(mask: "(00)00000-0000")
            let maskedStringTelefone = maskTelefone.mask(string:"(00)00000-0000");
            
            //let maskRazao = JMStringMask(mask: "00.000.000/0000-00")
            //let maskedStringRazao = maskRazao.mask(string:"60.703.722/0001-34");
            
            
            txtCpf.text = maskedStringCPF;
            txtTelefone.text = maskedStringTelefone;
            txtCnpj.text = maskedStringCNPJ;
            //txtRazao.text = maskedStringRazao;
            
            txtRazao.text = "asdasdsaduytqeqwe";


             txtNome.text = "Roque Junior";
             txtEmail.text = "mestreroque@safari.com";
            
             txtSenha.text = "111111";
             txtConfirmarSenha.text = "111111";
             txtEstado.text = "PE";
             txtEndereco.text = "Endereço teste";
            
       
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    @IBAction func onClickEstado(_ value: Any)
    {
         let actionSheetController: UIAlertController = UIAlertController(title:nil, message: "Estados", preferredStyle: .alert);

         let names:[String] = ["AC","AL","AP","AM","BA","CE","DF","ES","GO","MA","MT","MS","MG","PA","PB","PR","PE","PI","RJ","RN","RS","RO","RR","SC","SP","SE","TO"];
               
               
           for name in names
           {
             
               let item: UIAlertAction = UIAlertAction(title: name, style: .default) { action -> Void in
                   
                   self.txtEstado.text = name;
               }
             
               actionSheetController.addAction(item)

           }
        
            let item2: UIAlertAction = UIAlertAction(title:"Cancelar", style: .cancel) { action -> Void in
                
            }
        
            actionSheetController.addAction(item2)

           
           self.present(actionSheetController, animated: true, completion: nil)
         
     }
     
    
    @IBAction func onClickClose(sender: Any) {
        
        dismiss(animated: true, completion: nil);
        
    }

    
    
    @IBAction func onClickLogin(_ sender: Any) {
        
        self.initModel();
        
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if textField.tag < 9 {
            
        } else {
            textField.resignFirstResponder()
            initModel();
        }
        
        return false
    }
    
    static  var p:[String:Any] = [String:Any]();

    
    func initModel()
    {
  
        view.endEditing(false);

        if(!isOk())
        {
            return;
        }
        
        
        Cadastro.p["nome"] = txtNome.text!;
        Cadastro.p["email"] = txtEmail.text!;

        
        Cadastro.p["cnpj"] = txtCnpj.text!;
        Cadastro.p["cpf"] = txtCpf.text!;
        Cadastro.p["razao"] = txtRazao.text!;
        Cadastro.p["password"] = txtSenha.text!;

        Cadastro.p["profile_id"] = "1"
        Cadastro.p["password"] = txtSenha.text!;
        Cadastro.p["telefone"] = txtTelefone.text!;
        
        Cadastro.p["cep"] = "";
        Cadastro.p["estado"] = txtEstado.text!;
        Cadastro.p["cidade"] = "";
        Cadastro.p["bairro"] = "";
        Cadastro.p["endereco"] = txtEndereco.text!;
        Cadastro.p["complemento"] = "";
        
    
        let userId = OneSignal.getPermissionSubscriptionState().subscriptionStatus.userId ?? "-1"
        Cadastro.p["push"] = userId;

        self.present(AppDelegate.STAGE.getCadastro2(), animated: true, completion: nil);

        /*
        
        HTTP.request(URL:"\(HTTP.SERVER)",params:p,isLoad:true,completion:{json,status in
            
            //print(json.description);
            self.present(AppDelegate.STAGE.getCadastro2(), animated: true, completion: nil);
        
            
        });
 
         */
     
        
    }
    
    func isOk()->Bool
    {
        
        
        if(txtNome.text!.count <= 0)
        {
            addAlertOK(value: "Nome inválido");
            return false;
        }
        
        if(txtEmail.text!.count <= 0)
        {
            addAlertOK(value: "E-mail inválido");
            return false;
        }
        
        if(txtRazao.text!.count <= 0)
        {
            addAlertOK(value: "Razão inválida");
            return false;
        }
        
        if(txtCpf.text!.count <= 0)
        {
            addAlertOK(value: "CPF inválido");
            return false;
        }
        
        if(txtSenha.text!.count <= 0)
        {
            addAlertOK(value: "Senha inválida");
            return false;
        }
        
        if(txtSenha.text!.count < 6)
        {
            addAlertOK(value: "Senha inválida: ao menos 6 dígitos.");
            return false;
        }
        
        if(txtSenha.text! != txtConfirmarSenha.text!)
        {
            addAlertOK(value: "Senhas diferentes.");
            return false;
        }
        
        if(txtConfirmarSenha.text!.count <= 0)
        {
            addAlertOK(value: "Senha inválida");
            return false;
        }
        
        if(txtTelefone.text!.count <= 0)
        {
            addAlertOK(value: "Telefone inválido");
            return false;
        }
        
        if(txtCnpj.text!.count <= 0)
        {
            addAlertOK(value: "CNPJ inválida");
            return false;
        }
        
        if(txtEstado.text!.count <= 0)
        {
            addAlertOK(value: "Estado inválido");
            return false;
        }
        
        if(txtEndereco.text!.count <= 0)
        {
            addAlertOK(value: "Endereço inválido");
            return false;
        }

        return true;
        
    }
    
    
    
}
