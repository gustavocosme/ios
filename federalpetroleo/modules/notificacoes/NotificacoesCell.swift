//
//  PetsCell.swift
//  zoapet
//
//  Created by Gustavo Cosme on 08/02/17.
//  Copyright © 2017 SafariStudio. All rights reserved.
//

import UIKit
import SwiftyJSON
import MGSwipeTableCell

class NotificacoesCell: MGSwipeTableCell {
    
    
    
    @IBOutlet weak var titulo              :UILabel!;

    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
   
    func initJson(json:JSON)
    {
        titulo.text = json["texto"].string!;
    }
 
    
}//END CLASS
