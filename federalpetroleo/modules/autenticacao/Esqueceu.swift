//
//  LoginViewController.swift
//  vendaseuingresso
//
//  Created by Gustavo Cosme on 29/08/17.
//  Copyright © 2017 Safari. All rights reserved.
//

import UIKit
import PKHUD
import SwiftyJSON;
import OneSignal;

class Esqueceu: UIViewController, UITextFieldDelegate {
    
    //###################################################//
    //MARK:MODEL
    //###################################################//
    
    @IBOutlet weak var txtEmail: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
                
        txtEmail.delegate = self;
        txtEmail.tag = 0;
        
        if(AppDelegate.IS_TESTE)
        {
            txtEmail.text = "roqueroque@roque.com";
         
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    

    @IBAction func onClickEsqueceu(sender: Any) {
        
        
    }
    
    
    @IBAction func onClickLogin(_ sender: Any) {
        
        self.initModel();
        
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if textField.tag < 2 {
            
        } else {
            textField.resignFirstResponder()
            initModel();
        }
        
        return false
    }
    
    func initModel()
    {
  
        if(!isOk())
        {
            return;
        }
        
        var p:[String:Any] = [String:Any]();
        p["email"] = txtEmail.text!;
        p["profile_id"] = "1"
        
        let userId = OneSignal.getPermissionSubscriptionState().subscriptionStatus.userId ?? "-1"
        p["push"] = userId;

        HTTP.request(URL:"\(HTTP.SERVER)user/esqueceu-senha",params:p,isLoad:true,completion:{json,status in
        
            
            if(json["message"] != JSON.null)
            {
                self.addAlertOK(value:json["message"].string!)

            }else{
                
                self.addAlertOK(value:"Estamos melhorando nossos serviços.")

            }
            
            //self.addAlertOK(value:"E-mail enviado com sucesso!");

        });
        
    }
    
    func isOk()->Bool
    {
        if(txtEmail.text!.count <= 0)
        {
            addAlertOK(value: "E-mail inválido");
            return false;
        }
        
        if(!FormsUtils.isValidEmail(testStr: txtEmail.text!))
        {
            addAlertOK(value: "E-mail inválido");
            return false;
        }
        
        return true;
        
    }
    
    @IBAction func onClickClose(sender: Any) {
        
        dismiss(animated: true, completion: nil);
        
    }
    
    
    
}
