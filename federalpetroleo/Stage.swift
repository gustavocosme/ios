//
//  Stage.swift
//  opovoalerta
//
//  Created by Gustavo Cosme on 17/07/17.
//  Copyright © 2017 Guga. All rights reserved.
//

import UIKit
import Foundation

class Stage: NSObject {
    
    var storyboardMain                                  :UIStoryboard!
    
    override init()
    {
        storyboardMain      = UIStoryboard(name: "Main", bundle: nil)
    }
    
    func getNavAutenticacao() -> UINavigationController
    {
        return storyboardMain.instantiateViewController(withIdentifier:"NavLogin") as! UINavigationController;
    }
    
    func getNavMain() -> UINavigationController
    {
        return storyboardMain.instantiateViewController(withIdentifier:"NavMain") as! UINavigationController;
    }
    
    func getLogin() -> Login
    {
        return storyboardMain.instantiateViewController(withIdentifier:"Login") as! Login;
    }
    
    func getEsqueceu() -> Esqueceu
    {
        return storyboardMain.instantiateViewController(withIdentifier:"Esqueceu") as! Esqueceu;
    }
    
    func getCadastro2() -> Cadastro2
    {
        return storyboardMain.instantiateViewController(withIdentifier:"Cadastro2") as! Cadastro2;
    }
    
    func getCadastro() -> Cadastro
    {
        return storyboardMain.instantiateViewController(withIdentifier:"Cadastro") as! Cadastro;
    }
    
    func getSenhas() -> Senhas
    {
        return storyboardMain.instantiateViewController(withIdentifier:"Senhas") as! Senhas;
    }
    
    func getMain() -> Main
    {
       return storyboardMain.instantiateViewController(withIdentifier:"Main") as! Main;
    }
    
    func getAviso() -> Aviso
    {
       return storyboardMain.instantiateViewController(withIdentifier:"Aviso") as! Aviso;
    }
    
    func getUploadDocViewController() -> UploadDocViewController
    {
       return storyboardMain.instantiateViewController(withIdentifier:"UploadDocViewController") as! UploadDocViewController;
    }
    
    func getDialogPostos() -> DialogPostos
    {
       return storyboardMain.instantiateViewController(withIdentifier:"DialogPostos") as! DialogPostos;
    }
    
    func getDocumentacao() -> Documentacao
    {
       return storyboardMain.instantiateViewController(withIdentifier:"Documentacao") as! Documentacao;
    }
    
    func getDocumentosDebito() -> DocumentosDebito
    {
       return storyboardMain.instantiateViewController(withIdentifier:"DocumentosDebito") as! DocumentosDebito;
    }
    
    func getDocumentosCredito() -> DocumentosCredito
    {
       return storyboardMain.instantiateViewController(withIdentifier:"DocumentosCredito") as! DocumentosCredito;
    }
    
    func getCotacao() -> Cotacao
    {
       return storyboardMain.instantiateViewController(withIdentifier:"Cotacao") as! Cotacao;
    }
    
    func getFale() -> Fale
    {
       return storyboardMain.instantiateViewController(withIdentifier:"Fale") as! Fale;
    }
    
    func getNotificacoes() -> Notificacoes
    {
       return storyboardMain.instantiateViewController(withIdentifier:"Notificacoes") as! Notificacoes;
    }
    
    func getCotacaoFinal() -> CotacaoFinal
    {
       return storyboardMain.instantiateViewController(withIdentifier:"CotacaoFinal") as! CotacaoFinal;
    }
    
}//END CLASS


