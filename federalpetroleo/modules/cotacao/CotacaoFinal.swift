//
//  Pets.swift
//  zoapet
//
//  Created by Gustavo Cosme on 05/02/19.
//  Copyright © 2019 SafariStudio. All rights reserved.
//


//MARK: IMPORTS

import UIKit
import SwiftyJSON;

class CotacaoFinal: GCViewController, UITableViewDelegate ,UITableViewDataSource {
    
    
    //MARK: VAR

    var dados                           :[DataCotacao] = [DataCotacao]();
    @IBOutlet weak var tableView: UITableView!
    let refreshControl = UIRefreshControl();
    
    @IBOutlet weak var total:UILabel!
    @IBOutlet weak var btnFale:UIButton!

    
    //MARK: INIT

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //tableView.rowHeight = UITableView.automaticDimension;
        //tableView.estimatedRowHeight = 77.0;
        //tableView.tableFooterView = UIView(frame: CGRect.zero);
        
        tableView.backgroundColor = .white;
        view.backgroundColor = .white;
        
        
        var valorFinal:Double = 0.0
        
        for i in dados
        {
            let a = i.valor.replace(target: "R$ ", withString:"").replace(target: ",", withString: ".").doubleValue;
            let b = i.litros * a!;
            valorFinal += b;
        }
    
        let currencyFormatter = NumberFormatter()
        currencyFormatter.usesGroupingSeparator = true
        currencyFormatter.numberStyle = .currency
        currencyFormatter.locale = Locale(identifier: "pt_BR")
        let priceString = currencyFormatter.string(from: NSNumber(value:valorFinal))!
        total.text          = "\(priceString)";
        
        
    }
    
    @IBAction func onClickFale(_ e:Any)
    {
        let urlWhats = "https://wa.me/55\(DataUser.CURRENT.postoAtual["telefone"].string!)";

             if let urlString = urlWhats.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) {
                 if let whatsappURL = URL(string: urlString) {
                     if UIApplication.shared.canOpenURL(whatsappURL) {
                         UIApplication.shared.openURL(whatsappURL)
                     } else {
                         
                         print("INSTALE O ZAP");
                         
                     }
                 }
             }
    }
    
    //MARK: TABLEVIEW
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dados.count;
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 183;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CotacaoRespCell") as! CotacaoRespCell;
        cell.inicializa(data: dados[indexPath.row]);
        return cell;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }
    
  

    @IBAction func onClickClose(_ e:Any)
    {
        dismiss(animated: true, completion: nil);
    }
    
  
    
    
}//END CLASS
