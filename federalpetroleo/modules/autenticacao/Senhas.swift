//
//  LoginViewController.swift
//  vendaseuingresso
//
//  Created by Gustavo Cosme on 29/08/17.
//  Copyright © 2017 Safari. All rights reserved.
//

import UIKit
import PKHUD
import SwiftyJSON;
import OneSignal;

class Senhas: UIViewController, UITextFieldDelegate {
    
    //###################################################//
    //MARK:MODEL
    //###################################################//
    
    @IBOutlet weak var txtSenha: UITextField!
    @IBOutlet weak var txtSenha2: UITextField!


    override func viewDidLoad() {
        super.viewDidLoad()
                
        txtSenha.delegate = self;
        txtSenha2.tag = 1;
        
        if(AppDelegate.IS_TESTE)
        {
            txtSenha2.text = "1234";
            txtSenha.text = "1234";
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    @IBAction func onClickClose(sender: Any) {
        
        dismiss(animated: true, completion: nil);
        
    }
        
    @IBAction func onClickLogin(_ sender: Any) {
        
        self.initModel();
        
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if textField.tag < 2 {
            
        } else {
            textField.resignFirstResponder()
            initModel();
        }
        
        return false
    }
    
    func initModel()
    {
  
        if(!isOk())
        {
            return;
        }
        
        var p:[String:Any] = [String:Any]();
        p["senha"] = txtSenha.text!;
        p["confirmar_senha"] = txtSenha.text!;
        p["token"] = DataUser.tokenAux;

        p["profile_id"] = "1"
        
        let userId = OneSignal.getPermissionSubscriptionState().subscriptionStatus.userId ?? "-1"
        p["push"] = userId;

        HTTP.request(URL:"\(HTTP.SERVER)user/atualizar-senha",params:p,isLoad:true,completion:{json,status in
            
            if(json["status"].bool!)
            {
                DataUser.save(json: json,token:json["data"]["token"].string!);
                self.present(AppDelegate.STAGE.getMain(), animated: true, completion: nil);
            }
            else{
                
                self.addAlertOK(value: "Inválido!")
                
            }
                      
            
        });
        
        
        
     
        
    }
    
    func isOk()->Bool
    {
       
        
        if(txtSenha.text!.count <= 0)
        {
            addAlertOK(value: "Senha inválida");
            return false;
        }
        
        return true;
        
    }
    
    
    
}
