import UIKit

class DataTimeUtils: NSObject {
    
    
    
    static func timeStamp()->String
    {
        let date = NSDate()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy' 'HH:mm:ss"
        //formatter.timeZone = NSTimeZone(forSecondsFromGMT: 0)
        //formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX")
        return formatter.string(from: date as Date);
    }
    
    static func timeStampSaveImg()->String
    {
        let date = NSDate()
        let formatter = DateFormatter()
        formatter.dateFormat = "HHmmss"
        //formatter.timeZone = NSTimeZone(forSecondsFromGMT: 0)
        //formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX")
        return formatter.string(from: date as Date);
    }
    
}
