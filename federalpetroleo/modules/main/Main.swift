//
//  Main.swift
//  federalpetroleo
//
//  Created by Gustavo Cosme on 12/06/20.
//  Copyright © 2020 Safari. All rights reserved.
//

import UIKit

class Main: UIViewController {

    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .darkContent
    }
    
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var bgLine: UIImageView!
    @IBOutlet weak var postoTxt: UIButton!
    @IBOutlet weak var postoCNPJ: UILabel!
    
    static var INSTANCE:Main!;

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.bgLine.alpha = 0.0
        self.postoTxt.alpha = 0.0

        UIView.animate(withDuration: 3.5,animations: {
                         
                   self.bgLine.alpha = 1.0
                   self.postoTxt.alpha = 1.0


               }, completion: { bool in
                   
            })
        
        Main.INSTANCE = self;
        
        initModel();

    }

    @IBAction func onClickLogout(_ v:Any)
    {
          
          let actionSheetController: UIAlertController = UIAlertController(title:nil, message: "Gostaria de sair?", preferredStyle: .alert)
          
          let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
              
          }
          
          let okAction: UIAlertAction = UIAlertAction(title: "Ok", style: .default) { action -> Void in
              
              
              DataUser.delete();
              self.present(AppDelegate.STAGE.getNavAutenticacao(), animated: true, completion: nil);
              
              
          }
          
          actionSheetController.addAction(cancelAction)
          actionSheetController.addAction(okAction)
          self.present(actionSheetController, animated: true, completion: nil)
          
          
    }
      
    @IBAction func onClickMenu(_ v:Any)
    {
        let v = AppDelegate.STAGE.getDialogPostos();
        v.type = 0;
        
        v.onSelect = { value in
            
            self.postoCNPJ.text = DataUser.CURRENT.postoAtual["cnpj"].string!;
            self.postoTxt.setTitle(DataUser.CURRENT.postoAtual["name"].string!, for: .normal);
            
        }
        
        present(v, animated: true, completion: nil);
        
        
    }
    
    func initModel()
    {
        
   
        DataUser.selectPostoAtual(call: {
            
            self.postoCNPJ.text = DataUser.CURRENT.postoAtual["cnpj"].string!;
            self.postoTxt.setTitle(DataUser.CURRENT.postoAtual["name"].string!, for: .normal);
      
        })
        
        
        
    }
    
    
    
}//END CLASS
