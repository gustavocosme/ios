//
//  Pets.swift
//  zoapet
//
//  Created by Gustavo Cosme on 05/02/19.
//  Copyright © 2019 SafariStudio. All rights reserved.
//


//MARK: IMPORTS

import UIKit
import SwiftyJSON;

class Cotacao: GCViewController, UITableViewDelegate ,UITableViewDataSource {
    
    
    //MARK: VAR

    var dados                           :[DataCotacao] = [DataCotacao]();
    @IBOutlet weak var tableView: UITableView!
    let refreshControl = UIRefreshControl();
    
    var jsonPosto:JSON!;
    var jsonBase:JSON!;
    var jsonPrecos                           :[JSON] = [JSON]();
    var jsonArrayPostos                      :[JSON] = [JSON]();

    @IBOutlet weak var txtPosto: UILabel!
    @IBOutlet weak var txtBase : UILabel!

    @IBOutlet weak var btnRefazer : UIButton!
    @IBOutlet weak var btnCotacao : UIButton!
    @IBOutlet weak var btnFale    : UIButton!
    @IBOutlet weak var zap    : UIImageView!

    static var INSTANCE:Cotacao!;
    
    @IBOutlet weak var txtTotal : UILabel!
    @IBOutlet weak var txtTotal2 : UILabel!
    
    @IBOutlet weak var tv1 : UILabel!
    @IBOutlet weak var tv2 : UILabel!
    @IBOutlet weak var tv3 : UILabel!


    var isValorRender = false;
    
    //MARK: INIT

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //tableView.rowHeight = UITableView.automaticDimension;
        //tableView.estimatedRowHeight = 77.0;
        //tableView.tableFooterView = UIView(frame: CGRect.zero);
        
        tableView.backgroundColor = .white;
        view.backgroundColor = .white;
        
        tableView.alpha = 0;
        zap.isHidden = true;

        initUpdateRefresc();
        
        initModel();
        Cotacao.INSTANCE = self;
        
        
        
    }
    
    //MARK: UPDATE SWIPE
    
    
    func initUpdateRefresc(){
        
        let tableViewController = UITableViewController()
        tableViewController.tableView = tableView
        tableViewController.refreshControl = refreshControl;
        refreshControl.tintColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1);
        refreshControl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1);
        refreshControl.addTarget(self, action:#selector(onUpdateRefresc), for: UIControl.Event.valueChanged)
        
    }
    
    @objc func onUpdateRefresc()
    {
        initModel();
    }
    
    //MARK: TABLEVIEW
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return dados.count;
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
  
        if isValorRender
        {
            return 183;
        }
        
        return 70;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if !isValorRender {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CotacaoCell") as! CotacaoCell;
            cell.inicializa(data: dados[indexPath.row]);
            return cell;
            
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CotacaoRespCell") as! CotacaoRespCell;
        cell.inicializa(data: dados[indexPath.row]);
        return cell;
      
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }
    
    //MARK: MODEL
    
    func initModel(){
        
        var p       = [String:Any]();
        p["token"]  = DataUser.CURRENT.token;

        HTTP.request(URL:"https://federal-petroleo.safarihost.com.br/api/postos-precos",params:p,isLoad:true, completion:{json,status in
            
            if(status == "")
            {
                if(json["data"] == JSON.null)
                {
                    return;
                }
                
                print(json["data"].description);
                self.onReset();
                self.refreshControl.endRefreshing();
                self.jsonArrayPostos = json["data"]["posto"].array!
                
                self.tableView.alpha = 0;
                UIView.animate(withDuration: 1.5) {
                    self.tableView.alpha = 1.0
                }
                
                for i in self.jsonArrayPostos
                {
                    if(i["id"].int! == DataUser.CURRENT.postoAtual["id"].int!)
                    {
                        self.txtPosto.text = i["nome"].string!;
                        self.txtBase.text = "Base";
                        self.jsonPrecos = i["precos"].array!
                        self.jsonPosto = i;
                        self.jsonBase = nil;
                    }
                }
                
                /*
                self.txtPosto.text = self.jsonArrayPostos[0]["nome"].string!;
                self.txtBase.text = "Base";
                self.jsonPrecos = self.jsonArrayPostos[0]["precos"].array!

                self.jsonPosto = self.jsonArrayPostos[0];
                self.jsonBase = nil;
                self.onRender(array:self.jsonArrayPostos[0]["combustiveis"].array!)
                 */

            }
            
        });
        
    }
    
    func onReset()
    {
        //self.txtTotal.isHidden  = true;
        //self.txtTotal2.isHidden = true;
        
        //self.tv1.isHidden  = false;
        //self.tv2.isHidden = false;
        //self.tv3.isHidden = false;

        //self.txtPosto.text = "Posto";
        self.txtBase.text = "Base";
        //self.jsonPosto = nil;
        self.jsonBase = nil;
    
        Cotacao.INSTANCE.isValorRender = false;
        
        dados.removeAll();
        tableView.reloadData();
        
        tableView.alpha = 0;
        UIView.animate(withDuration: 1.5) {
            self.tableView.alpha = 1.0
        }
        
        tableView.reloadData();
        
        self.zap.isHidden = true;

        self.btnFale.isHidden = true;
        //self.btnRefazer.isHidden = true;
        self.btnCotacao.isHidden = false;
        
    }
    
    func initModelCotacao(){
        
        var p       = [String:Any]();
        p["token"] = DataUser.CURRENT.token;
    
        var aux = [DataCotacao]();
        
        for i in dados
        {
            if(i.diaInt > 0 && i.litrosStr != "")
            {
                aux.append(i);
            }
        }
        
        if(aux.count<=0)
        {
            addAlertOK(value:"Selecione pelo menos uma cotação corretamento com dias e litros!");
            return;
        }
        
        var pl = "";
        var pc = "";
        var pd = "";
        var pt = 0
        
        for i in aux
        {
            if(pt == aux.count-1)
            {
                pc += pc+"\(i.idCombustivel)"
                pd += pd+"\(i.diaInt)"
                pl += pd+"\(i.litrosStr)"
            }else{
                pc += pc+"\(i.idCombustivel),"
                pd += pd+"\(i.diaInt),"
                pl += pd+"\(i.litrosStr),"
            }
            
            pt = pt+1;
            
        }

        p["combustivel_id"] = pc;
        p["dias"] = pd;
        p["litros"] = pl;
        p["posto_id"] = String(DataUser.CURRENT.id)

        print("URL: https://federal-petroleo.safarihost.com.br/api/salvar-orcamento")
        print("TOKEN: \( DataUser.CURRENT.token)")
        print("combustivel_id: \(pc)")
        print("dias: \(pd)")
        print("litros: \(pl)")

        HTTP.request(URL:"https://federal-petroleo.safarihost.com.br/api/salvar-orcamento",params:p,isLoad:true, completion:{json,status in
            
            if(status == "")
            {
                
                if(json["data"] == JSON.null)
                {
                    return;
                }
                
                print(json.description);
                //self.addAlertOK(value:"Cotação realizada com sucesso!");
                self.refreshControl.endRefreshing();
                self.onRenderCotacao();
                
            }
            
        });
        
    }
    
    
    func onRender(array:[JSON])
    {
        
        if(array.count <= 0)
        {
            return;
        }
        
        dados.removeAll();
        tableView.reloadData();
        
        var p = 0;
        
        for json in array
        {
            let data = DataCotacao();
            data.titulo = json["titulo"].string!;
            data.valor = "R$ -";
            data.diaStr = "Selecione -"
            data.diaInt = 0;
            data.posicao = p;
            data.idCombustivel = json["id"].int!;
            self.dados.append(data);
        
            p = p+1;
            
        }
        
        tableView.alpha = 0;
        UIView.animate(withDuration: 1.5) {
            self.tableView.alpha = 1.0
            
        }
        
        tableView.reloadData();
        
        self.zap.isHidden = true;

        self.btnFale.isHidden = true;
        //self.btnRefazer.isHidden = true;
        self.btnCotacao.isHidden = false;
        
        
        
    }
    
    func onUpdateLitros(posicao:Int,litros:Double,listrosStr:String)
    {
        dados[posicao].litros       = litros;
        dados[posicao].litrosStr    = listrosStr;
        
        //tableView.reloadData();
        //onRenderCotacao();
    }
    
    func onUpdateDias(posicao:Int,dias:String,posicaoDia:Int)
    {
        dados[posicao].diaStr = dias;
        dados[posicao].diaInt  = jsonPrecos[posicaoDia]["dia"].int!;
        
        //print("DIA: \(dados[posicao].diaInt)");
        //print("COMBUSTIVEL: \(dados[posicao].titulo)");
        //print("ID COMBUSTIVEL: \(dados[posicao].idCombustivel)");

        for i in jsonPrecos[posicaoDia]["combustivel"].array!
        {
            if(i["id"].int! == dados[posicao].idCombustivel)
            {
                dados[posicao].valor  = i["valor"].string!;
            }
        
        }
        
        
        tableView.reloadData();
        //onRenderCotacao();
    }
    
    func onRenderCotacao()
    {
        //self.btnFale.isHidden = false;
        self.btnRefazer.isHidden = false;
        self.zap.isHidden = false;
        
        var array = [DataCotacao]()
        
        for i in dados
        {
            if(i.litrosStr != "" && i.diaStr != "Selecione -")
            {
                array.append(i);
            }
        }
        
        let v = AppDelegate.STAGE.getCotacaoFinal();
        v.dados = array
        present(v, animated: true, completion: nil);
        
        /*
        
        Cotacao.INSTANCE.isValorRender = true;
        
        self.btnFale.isHidden = false;
        self.btnRefazer.isHidden = false;
        self.zap.isHidden = false;
        
        txtTotal.isHidden = false;
        txtTotal2.isHidden = false;
        
        self.tv1.isHidden  = true;
        self.tv2.isHidden = true;
        self.tv3.isHidden = true;

        self.btnCotacao.isHidden = true;
        
        tableView.alpha = 0;

        UIView.animate(withDuration: 1.5) {
            self.tableView.alpha = 1.0
            
        }
        
        tableView.reloadData();
        //Cotacao.INSTANCE.isValorRender = false;
 
         */

    }
    
    
    //MARK: CLICKS

    @IBAction func onClickCotacao(_ e:Any)
    {
        //initModelCotacao();
        
        if(jsonPosto == nil || jsonBase == nil)
        {
            addAlertOK(value: "Selecione o posto e a base!");
            return;
        }
        
        initModelCotacao();
    }

    @IBAction func onClickClose(_ e:Any)
    {
        dismiss(animated: true, completion: nil);
    }
    
    @IBAction func onClickPosto(_ e:Any)
    {
        let v = AppDelegate.STAGE.getDialogPostos();
        v.type = 1;
        v.dados = jsonArrayPostos;
             
        v.onSelect = { value in
             
            self.txtPosto.text = value["nome"].string!;
            self.txtBase.text = "Base";
            self.jsonPrecos = value["precos"].array!

            self.jsonPosto = value;
            self.jsonBase = nil;

         }
             
         present(v, animated: true, completion: nil);
    }
    
    @IBAction func onClickBase(_ e:Any)
    {
      
        if(jsonPosto == nil)
        {
            addAlertOK(value:"Selecione o posto");
            return;
        }
        
        let v = AppDelegate.STAGE.getDialogPostos();
        v.type = 2;
        v.dados = jsonPosto["bases"].array!;

        v.onSelect = { value in
             
            self.txtBase.text = value["nome"].string!;
            self.jsonBase = value;
            self.onRender(array:self.jsonPosto["combustiveis"].array!)
  
         }
             
         present(v, animated: true, completion: nil);
        
    }
    
    @IBAction func onClickFale(_ e:Any)
    {
        let urlWhats = "https://wa.me/55\(DataUser.CURRENT.postoAtual["telefone"].string!)";

             if let urlString = urlWhats.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) {
                 if let whatsappURL = URL(string: urlString) {
                     if UIApplication.shared.canOpenURL(whatsappURL) {
                         UIApplication.shared.openURL(whatsappURL)
                     } else {
                         
                         print("INSTALE O ZAP");
                         
                     }
                 }
             }
    }
    
    @IBAction func onClickReFazer(_ e:Any)
    {
        onReset();
    }
    
    
    
}//END CLASS
