//
//  PetsCell.swift
//  zoapet
//
//  Created by Gustavo Cosme on 08/02/17.
//  Copyright © 2017 SafariStudio. All rights reserved.
//

import UIKit
import SwiftyJSON


class NotificacoesCellData: UITableViewCell {
    
    @IBOutlet weak var titulo              :UILabel!;

    override func awakeFromNib() {
        
        super.awakeFromNib()
        
    }
 
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
   
    func set(value:String)
    {
        titulo.text = value;
    }
  
}//END CLASS
