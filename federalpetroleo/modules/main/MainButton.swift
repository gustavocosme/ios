import UIKit;
import JMMaskTextField_Swift;
import Alamofire;


class DataMainButton:NSObject {

    var title = "";
    var icon:UIImage!;
    

}


@IBDesignable
class MainButton: UIView{
    
    @IBOutlet weak var txt         : UILabel!
    @IBOutlet weak var icon        : UIImageView!
    @IBOutlet weak var view        : UIView!
    
    
    @IBInspectable var title:String!{
        didSet{
            txt.text    = title;

        }
    }
    
    @IBInspectable var image:UIImage!{
        didSet{
            icon.image  = image;

        }
    }
    
    
    @IBInspectable var id:String!;

    
    override init(frame: CGRect) {
        super.init(frame: frame)

        nibSetup();
    }
    
  
    @IBAction func onClick(_ e:Any)
    {
        
        print("ON CLICK");
        
        if let topVC = UIApplication.getTopViewController() {
            
            topVC.view.endEditing(false);
            //topVC.present(controller, animated: true, completion: nil);
            
            if(id == "0")
            {
                topVC.present(AppDelegate.STAGE.getDocumentacao(), animated: true, completion: nil);
            }
            
            if(id == "1")
            {
                topVC.present(AppDelegate.STAGE.getCotacao(), animated: true, completion: nil);
            }
            
            if(id == "2")
            {
                topVC.present(AppDelegate.STAGE.getFale(), animated: true, completion: nil);
            }
            
            if(id == "3")
            {
                topVC.present(AppDelegate.STAGE.getNotificacoes(), animated: true, completion: nil);
            }
            
        
        }
        
        
        
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        nibSetup()
    }
    
    private func nibSetup() {
        backgroundColor = .clear
        
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.translatesAutoresizingMaskIntoConstraints = true
        addSubview(view)
        
    }
    
    private func loadViewFromNib() -> UIView {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let nibView = nib.instantiate(withOwner: self, options: nil).first as! UIView
        
        return nibView
    }
    
}



