//
//  HTTP.swift
//  casacorpe
//
//  Created by Safari Mobile on 24/08/2018.
//  Copyright © 2018 Safari Mobile. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import NotificationCenter;
import PKHUD

class HTTP: NSObject {
    
    
    public static var IS_DEBUG                                 = false;
    public static var SERVER_RELEASE                           = "https://federal-petroleo.safarihost.com.br/api/";
    public static var SERVER_DEBUG                             = "";
    private static var ERRO                                     = "Ops... Veja sua conexão :(";
    
    public static var SERVER                                   = HTTP.IS_DEBUG == true ?HTTP.SERVER_DEBUG:HTTP.SERVER_RELEASE;
    
    
    
    static func request(URL:String,params:[String:Any] = [String:Any](),isLoad:Bool = false,completion: @escaping (JSON,String)->Void)
    {
        
        URLCache.shared.removeAllCachedResponses()
        UIApplication.shared.isNetworkActivityIndicatorVisible = true;
        
        if let topVC = UIApplication.getTopViewController() {
            
            topVC.view.endEditing(false);
            
        }
        
        if(isLoad)
        {
            HUD.dimsBackground              = true;
            HUD.allowsInteraction           = false;
            
            if let topVC = UIApplication.getTopViewController() {
                
                HUD.show(.progress, onView: topVC.view);
                
            }
        }
    
        AF.request("\(URL)",method:.post,parameters:params).responseJSON { response in
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = false;
            
            if(isLoad)
            {
                HUD.hide();
            }
            
            switch response.result {
                
            case .success:
                
                do {
                    
                    let json = try JSON(data: response.data!);
                    
                    
                    if(json["status"] != JSON.null)
                    {
                        
                        if(!json["status"].bool!)
                        {
                            if let topVC = UIApplication.getTopViewController() {
                                
                                if(json["message"] != JSON.null)
                                {
                                    if(json["message"].string! == "Acesso não permitido. ")
                                    {
                                        DataUser.delete();
                                        topVC.present(AppDelegate.STAGE.getNavAutenticacao(), animated: true, completion: nil);
                                        
                                        topVC.addAlertOK(value:"Sessão expirada!");

                                        
                                    }else{
                                        
                                        topVC.addAlertOK(value:json["message"].string!);

                                    }
                                    
                                }
                                
                                
                            }
                            
                        }else{
                            
                            completion(json,"")

                            
                        }
                        
                        
                        
                    }else{
                        
                        completion(json,"")

                    }
                    
                    

                    
                } catch {
                    
                    completion(JSON(),error.localizedDescription);
                    
                    if let topVC = UIApplication.getTopViewController() {
                        
                        topVC.addAlertOK(value: HTTP.ERRO);
                        
                    }
                    
                }
                
            case .failure(let error):
                
                completion(JSON(),error.localizedDescription);
                
                if let topVC = UIApplication.getTopViewController() {
                    
                    topVC.addAlertOK(value: HTTP.ERRO);
                    
                }
                
            }
        }
        
        
    }
    
   
    
    
    
    
    
    
}//END CLASS
