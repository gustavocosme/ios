//
//  UploadImage.swift
//  federalpetroleo
//
//  Created by Gustavo Cosme on 16/08/20.
//  Copyright © 2020 Safari. All rights reserved.
//

import UIKit
import UIKit
import JMMaskTextField_Swift;
import PKHUD
import Alamofire
import SwiftyJSON;

class DataUploadImage: NSObject {
    
    var image:UIImage!;
    var nameParams  = "";

}

class UploadImage:NSObject {
    
    func upload(_ url: String, images: [DataUploadImage], params: [String : Any],completion: @escaping ((JSON) -> Void)) {
           
           URLCache.shared.removeAllCachedResponses()
           UIApplication.shared.isNetworkActivityIndicatorVisible = true;
           
           HUD.dimsBackground              = true;
           HUD.allowsInteraction           = false;
                   
           if let topVC = UIApplication.getTopViewController() {
                       
               HUD.show(.progress, onView: topVC.view);
                       
           }
        
           print(url);
           
           let httpHeaders = HTTPHeaders(["Content-Type":"application/json"])
        
           AF.upload(multipartFormData: { multiPart in
            
               for p in params {
                   multiPart.append("\(p.value)".data(using: String.Encoding.utf8)!, withName:p.key)
                
                    print("PARAMS NORMAL VALUE: \(p.value) - KEY: \(p.key)")
               
               }
            
                for i in images {
                    
                    let identifier = UUID()
                    multiPart.append(i.image.jpegData(compressionQuality: 1.0)!, withName: "\(i.nameParams)", fileName: "\(identifier.uuidString).jpg", mimeType: "image/jpg")
                    
                    print("PARAMS IMAGES VALUE: \(i.nameParams)");

                    
                }
               
           }, to: url, method: .post, headers: httpHeaders) .uploadProgress(queue: .main, closure: { progress in
               print("Upload Progress: \(progress.fractionCompleted)")
           }).responseJSON(completionHandler: { data in
               
               print("upload finished: \(data)")
           }).response { (response) in
               
               UIApplication.shared.isNetworkActivityIndicatorVisible = false;
               HUD.hide();

               do {
                   
                   let json = try JSON(data: response.data!);
                   print(json.description);
                
                    DispatchQueue.global().async {
                        completion(json);
                    }
                   
                   
               }catch {
                   
                    if let topVC = UIApplication.getTopViewController() {
                                      
                       topVC.addAlertOK(value: "Estamos melhorando nossos serviços");

                    }
                    
                    print(error.localizedDescription);
                   
               }
               
           }
       }

    
}
