//
//  UIVIewControllerGC.swift
//  livres
//
//  Created by Safari Mobile on 09/02/17.
//  Copyright © 2017 Safari. All rights reserved.
//

import UIKit

class GCViewController: UIViewController {

    
    var txtAviso:UILabel!;
    
    
    override func viewDidLoad() {
        super.viewDidLoad();
        
        
        addAviso();

    }
    
    private func addAviso()
    {
        
        txtAviso = UILabel(frame: CGRect(x:0.0, y: view.frame.height/4, width: view.frame.width, height: 100));
        txtAviso.textAlignment = .center;
        txtAviso.textColor = .darkGray;
        txtAviso.font = UIFont.boldSystemFont(ofSize: 14.0);
        
        view.addSubview(txtAviso);
        
    }
    
    public func showAviso(txt:String)
    {
        txtAviso.isHidden = false;
        txtAviso.text = txt;
    }
    
    public func hideAviso()
    {
        txtAviso.isHidden = true;
        txtAviso.text = "";
    }
    
}//END CLASS


extension UIViewController
{
    
    func setTitleInterna(value:String){
        
        self.navigationItem.title = value;
        let backButton = UIBarButtonItem()
        backButton.title = ""
        backButton.tintColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1);
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        self.navigationItem.rightBarButtonItems = [];

    }
    
    /*
    func addMenuSlide()
    {
        
        let button:UIBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "icon_menu_32"), style:.plain, target: nil, action:#selector(onSelectMenu));
        navigationItem.leftBarButtonItems = [button];
        
        
    }
     */
    
 
 
    
    func delay(_ delay: Double, closure:@escaping () -> Void) {
        DispatchQueue.main.asyncAfter(
            deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
    }
 
     
    
    func hideKeyboardWhenTappedAround()
    {
    
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboardView")
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
   
    }
    
    func dismissKeyboardView() {
        view.endEditing(true)
    }
    
    func addAlertOK(value:String)
    {
        let alertController = UIAlertController(title:nil, message: value, preferredStyle: .alert)
        let OKAction = UIAlertAction(title:"OK", style: .default, handler: nil)
        alertController.addAction(OKAction)
        present(alertController, animated: true, completion: nil)
        
    }
    
  
    
    
    
}//END CLASS
