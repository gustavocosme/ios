//
//  DocumentosDebito.swift
//  federalpetroleo
//
//  Created by Gustavo Cosme on 24/08/20.
//  Copyright © 2020 Safari. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import SwiftyJSON

class DocumentosDebito: UIViewController, IndicatorInfoProvider{

    var dados:[JSON] = [JSON]();
    @IBOutlet weak var quadro: UploadDocViewButton!
    @IBOutlet weak var contrato: UploadDocViewButton!
    
    static var INTANCE:DocumentosDebito!;
    
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
         
         return IndicatorInfo(title: "  Á Vista  ")
         
     }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        DocumentosDebito.INTANCE = self;
        initModel();
        
        
    }
    
    func initModel()
    {
        
        print("initModel")
        
        var p:[String:Any] = [String:Any]();
        p["token"] = DataUser.CURRENT.token;
        p["posto_id"] = String(DataUser.CURRENT.id)

        HTTP.request(URL:"\(HTTP.SERVER)user/doc/a-vista",params:p,isLoad:true,completion:{json,status in
        
            if(json["status"].bool!){
                self.dados = json["data"].array!;
                self.onRender();
            }
            else{
                self.addAlertOK(value: "Inválido!")
            }
        });
        
        
        
    }
    
    func onRender()
    {
        var p = 0;
        
        for i in dados {
            
            if(dados[p]["status"].string! != "")
            {
            
            if(p == 0)
            {
                
                
                    if(i["status"].string == "aguardando")
                    {
                        
                        quadro.setStatus(status: .AGUARDANDO);
                        
                    }
                    if(i["status"].string == "liberado")
                    {
                        
                        quadro.datas.imageUrl = i["arquivo"].string!;
                        quadro.setStatus(status: .OK);
                        
                    }
                    else
                    {
                        
                        quadro.setStatus(status: .ERRO);
                        
                    }
                    
                    
                    
                }
                
                if(p == 1)
                {
                    if(i["status"].string == "aguardando")
                    {
                         contrato.setStatus(status: .AGUARDANDO);
                    }
                    else
                    if(i["status"].string == "liberado")
                    {
                         contrato.setStatus(status: .OK);
                    }
                    else
                    {
                         contrato.setStatus(status: .ERRO);
                    }
                     
                }
                
            }
             
            
            p += 1;
        }
        
        
    }

    

}//END CLASS
