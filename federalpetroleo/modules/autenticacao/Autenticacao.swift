//
//  Autenticacao.swift
//  federalpetroleo
//
//  Created by Gustavo Cosme on 10/08/20.
//  Copyright © 2020 Safari. All rights reserved.
//

import UIKit

class Autenticacao: UIViewController {

    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        if(UserDefaults.standard.object(forKey:"logado") != nil)
             {
                           
                 present(AppDelegate.STAGE.getNavMain(), animated: true, completion: nil)
                        
             }else{
                           
                        
             }
             

        
    }
    
    @IBAction func onClickEsqueceuSenha(_ value:Any){
           
           present(AppDelegate.STAGE.getEsqueceu(), animated: true, completion: nil);
           
           
    }
      
       
    @IBAction func onClickCadastro(_ value:Any){
           
           present(AppDelegate.STAGE.getCadastro(), animated: true, completion: nil);

    }
       
    @IBAction func onClickLogin(_ value:Any){
           
        present(AppDelegate.STAGE.getLogin(), animated: true, completion: nil);
    
    }
       
       

    
    
    
    

}//END CLASS
