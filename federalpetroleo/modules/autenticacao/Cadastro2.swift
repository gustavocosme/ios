//
//  Cadastro2.swift
//  federalpetroleo
//
//  Created by Gustavo Cosme on 10/08/20.
//  Copyright © 2020 Safari. All rights reserved.
//

import UIKit

class Cadastro2: UIViewController {
    
    
    @IBOutlet weak var uploadDoc1: UploadDocViewButton!
    @IBOutlet weak var uploadDoc2: UploadDocViewButton!


    override func viewDidLoad() {
        super.viewDidLoad()

        //let data1 = DataUploadDoc();
        //data1.title = "Adicione - Quadro ANP, Totem, Bombas, Testeira"
        //uploadDoc1.set(data: data1);
     
        
        //let data2 = DataUploadDoc();
        //data2.title = "Adicione - Contrato Social e/ou suas alterações"
        //uploadDoc2.set(data: data2);
        
        
    }
    
    @IBAction func onClickModel(sender: Any) {
        
        
        if(!uploadDoc1.isValidation())
        {
            addAlertOK(value: "Adicione o documento 1!");
            return;
        }
        
        if(!uploadDoc2.isValidation())
        {
            addAlertOK(value: "Adicione o documento 2!");
            return;
        }
        
        var p:[String:Any] = [String:Any]();
        p["profile_id"] = "1"
        
        let i1 = DataUploadImage();
        i1.nameParams = "quadro_file";
        i1.image = uploadDoc1.datas.imageDoc;
        
        let i2 = DataUploadImage();
        i2.nameParams = "contrato_social_file";
        i2.image = uploadDoc2.datas.imageDoc;
        
        Cadastro.p["titulo_quadro"] = String(uploadDoc1.txt.text!);
        Cadastro.p["titulo_contrato_social"] = String(uploadDoc2.txt.text!);

        print("\(HTTP.SERVER)user/cadastro");
        print(Cadastro.p);

        
        UploadImage().upload("\(HTTP.SERVER)user/cadastro", images:[i1,i2], params: Cadastro.p, completion: {json in
            
            DispatchQueue.global(qos: .utility).async {
                   
                       DispatchQueue.main.async {
                        
                                print(json.description);
                        
                         
                        
                                if(json["status"].bool!)
                                {
                                    //DataUser.save(json: json);
                                    //self.present(AppDelegate.STAGE.getMain(), animated: true, completion: nil);
                                    self.present(AppDelegate.STAGE.getAviso(), animated: true, completion: nil);
                                }
                                else{
                                    
                                    self.addAlertOK(value: json["message"].string!)
                                    
                                }
                        
                                //self.present(AppDelegate.STAGE.getAviso(), animated: true, completion: nil);
                        
                       }
                   }
           

        })
        
     
    }
    

     @IBAction func onClickClose(sender: Any) {
         
         dismiss(animated: true, completion: nil);
         
     }

}
