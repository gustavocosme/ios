//
//  DataEvent.swift
//  vendaseuingresso
//
//  Created by Gustavo Cosme on 23/08/17.
//  Copyright © 2017 Safari. All rights reserved.
//

import UIKit
import SwiftyJSON;

class DataNotificacoes: NSDictionary {
    
    var json                           :JSON!;
    var isSelect                       :Bool = false;
    var titulo = "";
    var isTitle                        :Bool = false;
    
}//END CLASS
