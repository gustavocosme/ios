//
//  NotificacaoUtils.swift
//  zoapet
//
//  Created by Gustavo Cosme on 12/03/19.
//  Copyright © 2019 SafariStudio. All rights reserved.
//

import UIKit
import NotificationCenter
import UserNotifications;


class NotificacaoUtils: NSObject {
    
    static func sendNotification() {
        
        let content = UNMutableNotificationContent()
        content.title = "Notification Tutorial"
        content.subtitle = "from ioscreator.com"
        content.body = " Notification triggered"
        
        let imageName = "ic_cachorro"
        guard let imageURL = Bundle.main.url(forResource: imageName, withExtension: "png") else { return }
        
        let attachment = try! UNNotificationAttachment(identifier: imageName, url: imageURL, options: .none)
        
        content.attachments = [attachment]
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 0, repeats: false)
        let request = UNNotificationRequest(identifier: "notification.id.01", content: content, trigger: trigger)
        
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
   
    
    }

}
