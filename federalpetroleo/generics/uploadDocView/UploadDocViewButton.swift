import UIKit;
import JMMaskTextField_Swift;
import Alamofire;

@IBDesignable
class UploadDocViewButton: UIView{
    
    @IBOutlet weak var view        : UIView!
    @IBOutlet weak var txt         : UILabel!
    @IBOutlet weak var icon        : UIImageView!
    
    var datas:DataUploadDoc = DataUploadDoc();
    
    @IBInspectable var typeSend: String? {
    
         didSet {
            datas.typeSend = typeSend!;

         }
     
    }
    
    @IBInspectable var url: String? {
   
        didSet {
            datas.urlPatch = url!;
        }
    
    }
    
    @IBInspectable var title: String? {
   
        didSet {
            datas.title = title!;
            txt.text = datas.title;
        }
    
    }
    
     @IBInspectable var maskS: String? {
    
         didSet {
             datas.mask = maskS!;
         }
     
     }
    
     @IBInspectable var params: String? {
    
         didSet {
            datas.paramsName = params!;
         }
     
     }
    
    @IBInspectable var paramsTitle: String? {
   
        didSet {
           datas.paramsTitle = paramsTitle!;
        }
    
    }
    
     @IBInspectable var placeHolder: String? {
    
         didSet {
            datas.placeHolder = placeHolder!;
         }
     
     }
    
 
    
    override init(frame: CGRect) {
        super.init(frame: frame)

        nibSetup();

    }
    
    func set(data:DataUploadDoc)
    {
        //self.datas = data;
        //txt.text = data.title;
        icon.isHidden = true;
    }
    
    @IBAction func onClick(_ e:Any)
    {
        
        if let topVC = UIApplication.getTopViewController() {
                   
            topVC.view.endEditing(false);
            
            var v = AppDelegate.STAGE.getUploadDocViewController();
            v.datas = self;
            topVC.present(v, animated: true, completion: nil)
            
        }
               
    }
    
    
    func setStatus(status:EnumUploadDoc)
    {
        
        icon.isHidden = false;

        if(status == EnumUploadDoc.AGUARDANDO)
        {
            icon.image = #imageLiteral(resourceName: "ic_form_aguardando")
        }
        
        if(status == EnumUploadDoc.OK)
        {
            icon.image = #imageLiteral(resourceName: "ic_form_ok")
        }
        
        if(status == EnumUploadDoc.ERRO)
        {
            icon.image = #imageLiteral(resourceName: "ic_form_erro")
        }
        
    }
    
    
    func isValidation()->Bool
    {
        
        
       
        
        return datas.isComplete;
    
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        nibSetup()
        
        icon.isHidden = true;

    }
    
    private func nibSetup() {
        backgroundColor = .clear
        
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.translatesAutoresizingMaskIntoConstraints = true
        addSubview(view)
        
    }
    
    private func loadViewFromNib() -> UIView {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let nibView = nib.instantiate(withOwner: self, options: nil).first as! UIView
        
        return nibView
    }
    
}



