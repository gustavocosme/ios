//
//  Pets.swift
//  zoapet
//
//  Created by Gustavo Cosme on 05/02/19.
//  Copyright © 2019 SafariStudio. All rights reserved.
//

import UIKit
import SwiftyJSON;


class DialogPostos: GCViewController, UITableViewDelegate ,UITableViewDataSource {
    
    var dados                           :[JSON] = [JSON]();
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var titleTxt: UILabel!

    var onSelect: ((JSON) -> Void)!
    var isSave = true;
    
    var type = 0;

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.register(UINib(nibName: "DialogPostoCell", bundle: nil), forCellReuseIdentifier: "DialogPostoCell")
      
        
        tableView.rowHeight = UITableView.automaticDimension;
        tableView.estimatedRowHeight = 77.0;
        tableView.tableFooterView = UIView(frame: CGRect.zero);
        
        
        tableView.backgroundColor   = .white
        view.backgroundColor        = .white

        if(type == 0)
        {
             initModel();
        }
        
        if(type == 2)
        {
            titleTxt.text = "Bases";
        }
        
        tableView.reloadData();
       
    }
    
    @IBAction func onClickClose(_ e:Any)
    {
        dismiss(animated: true, completion: nil);
    }
    

    //MARK: TABLEVIEW
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return dados.count;

    
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 55;
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        let cell = tableView.dequeueReusableCell(withIdentifier: "DialogPostoCell") as! DialogPostoCell;
        
        if(type == 0)
        {
            cell.initJson(json: dados[indexPath.row]);
        }
            
        if(type == 1)
        {
            cell.initJsonPostoOff(json: dados[indexPath.row]);
        }
        
        if(type == 2)
        {
            cell.initJsonBaseOff(json: dados[indexPath.row]);
        }
        
        return cell;
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        if(type == 0)
        {
            if(isSave)
            {
                DataUser.selectPostoAtual(call: {
                
                    print(self.dados[indexPath.row]["id"].int!);
                    
                    self.onSelect(self.dados[indexPath.row])
                    self.dismiss(animated: true, completion: nil);
              
                },idA:dados[indexPath.row]["id"].int!)
                
            }else{
                
                self.onSelect(self.dados[indexPath.row])
                self.dismiss(animated: true, completion: nil);
                
            }
        }
        
        if(type == 1 || type == 2)
        {
            self.onSelect(self.dados[indexPath.row])
            self.dismiss(animated: true, completion: nil);
        }
        
        
    }
    
    
    //MARK: MODEL
    
    func initModel()
    {
        
        var p:[String:Any] = [String:Any]();
        p["token"] = DataUser.CURRENT.token;
        
        //print(DataUser.CURRENT.token);
        //print("\(HTTP.SERVER)user/dados");
        
        HTTP.request(URL:"\(HTTP.SERVER)user/dados",params:p,isLoad:true,completion:{json,status in
         
            
            if(json["status"].bool!)
            {
            
                //print(json["data"]["usuario"]["postos"].array!.count);
                
                if(json["data"]["usuario"]["postos"].array!.count > 0)
                {
             
                    self.dados =  json["data"]["usuario"]["postos"].array!;
                    
                    self.tableView.alpha = 0;
                    UIView.animate(withDuration: 1.5) {
                           self.tableView.alpha = 1.0
                          
                    }
                    
                    self.tableView.reloadData();
                    
                }
            
                
            }
            else{
                
                self.addAlertOK(value: "Login inválido!")
                
            }
         
                      
            
        });
        
        
        
    }
    
    
    
    
 
    
  
    
    
    
}//END CLASS
