//
//  DataUser.swift
//  zoapetv
//
//  Created by Gustavo Cosme on 02/01/20.
//  Copyright © 2020 SafariStudio. All rights reserved.
//

import UIKit
import SwiftyJSON

class DataUser: NSObject {
    
    static var tokenAux = "";

    var idPosto = -1;

    var id = -1;
    var nome = "";
    var email = "";
    var token = "";
    
    var cep = "";
    var cpf = "";
    var cnpj = "";
    var endereco = "";
    var bairro = "";
    var cidade = "";
    var estado = "";
    var telefone = "";
    
    var postos = [JSON]();
    var postoAtual = JSON();


    static var CURRENT:DataUser!;

    override init() {
    
    }
    
    
    static func inicialize()->Bool
    {
        
        
        if(UserDefaults.standard.object(forKey:"user") == nil)
        {
        
            DataUser.CURRENT = nil;
            return false;
            
        }else{
        
            DataUser.CURRENT = DataUser();
            let json = JSON(parseJSON:Pref.get(key: "user") as! String);
            

            if(json["nome"] == JSON.null)
            {
                DataUser.CURRENT = nil;
                return false;
            }
            

            
            if(json["id"] != JSON.null)
            {
                DataUser.CURRENT.id = json["id"].int!;

            }
            
            if(json["nome"] != JSON.null)
            {
                DataUser.CURRENT.nome = json["nome"].string!;

            }
            
            if(json["email"] != JSON.null)
            {
                DataUser.CURRENT.email = json["email"].string!;

            }
            
            if(json["cep"] != JSON.null)
            {
                DataUser.CURRENT.cep = json["cep"].string!;

            }
            
            if(json["cpf"] != JSON.null)
            {
                DataUser.CURRENT.cpf = json["cpf"].string!;

            }
            
            if(json["cnpj"] != JSON.null)
            {
                DataUser.CURRENT.cnpj = json["cnpj"].string!;

            }
            
            if(json["endereco"] != JSON.null)
            {
                DataUser.CURRENT.endereco = json["endereco"].string!;

            }
            
            if(json["bairro"] != JSON.null)
            {
                DataUser.CURRENT.bairro = json["bairro"].string!;

            }
            
            if(json["cidade"] != JSON.null)
            {
                DataUser.CURRENT.cidade = json["cidade"].string!;

            }
            
            if(json["estado"] != JSON.null)
            {
                DataUser.CURRENT.estado = json["estado"].string!;

            }
            
            if(json["telefone"] != JSON.null)
            {
                DataUser.CURRENT.telefone = json["telefone"].string!;

            }
            
            if(json["postos"] != JSON.null)
            {
                //print(json["postos"].description);
                //DataUser.selectPostoAtual(id:json["postos"]["id"].int!,json:json["postos"].array!);
            }
            
            
            DataUser.CURRENT.token = Pref.get(key: "token") as! String;

            return true;

        }

        
    }
    
    static func selectPostoAtual(call:@escaping()->Void,idA:Int = -1)
    {
        var id = idA;

        if(id != -1)
        {
            Pref.add(params: String(id), key: "id");
        }
        
        if(UserDefaults.standard.object(forKey:"id") != nil)
        {
            id = Int(Pref.get(key: "id") as! String)!;
        }
        
        /*
        print("ID 1 \(id)")
         */
        
        var p:[String:Any] = [String:Any]();
        p["token"] = DataUser.CURRENT.token;
        
        print(DataUser.CURRENT.token);
        //print("\(HTTP.SERVER)user/dados");
        
        //print("ID 2 \(id)")
        
        HTTP.request(URL:"\(HTTP.SERVER)user/dados",params:p,isLoad:true,completion:{json,status in
         
            
            if(json["status"] == JSON.null)
            {
                if let topVC = UIApplication.getTopViewController() {
                    
                    DataUser.delete();
                    topVC.present(AppDelegate.STAGE.getNavAutenticacao(), animated: true, completion: nil);
                    topVC.addAlertOK(value:"Sessão expirada!");
                    
                }
                
                return;
            
            }
            
            if(json["status"].bool!)
            {
                            
                
                if(json["data"]["usuario"]["postos"].array!.count > 0)
                {
                    
                    if(id == -1)
                    {
                        DataUser.CURRENT.postoAtual = json["data"]["usuario"]["postos"].array![0];
                        call();
                        return;
                    }
                
                    for i in json["data"]["usuario"]["postos"].array!
                    {
                        if(i["id"].int! == id)
                        {
                            DataUser.CURRENT.postoAtual = i;
                            call();
                            print(DataUser.CURRENT.postoAtual["cnpj"].string!)
                        }
                        
                    }
                  
                    //self.postoCNPJ.text = DataUser.CURRENT.postoAtual["cnpj"].string!;
                    //self.postoTxt.setTitle(DataUser.CURRENT.postoAtual["name"].string!, for: .normal);
              
                    
                }
            
                
            }
            else{
                
                
            }
         
                      
            
        });
        
    }
    
    
    static func delete()
    {
        
        DataUser.CURRENT = nil;
        Pref.remove(key:"logado");
        Pref.remove(key:"token");
        Pref.remove(key:"token");
        Pref.remove(key:"user");

    }
    
    static func save(json:JSON,token:String = "")
    {
        
        
        Pref.add(params:"true", key:"logado");
        Pref.add(params:token, key:"token");
        Pref.add(params: json["data"]["usuario"].description, key:"user");
        inicialize();
    }
    
    
    
}
