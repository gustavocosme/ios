//
//  Pets.swift
//  zoapet
//
//  Created by Gustavo Cosme on 05/02/19.
//  Copyright © 2019 SafariStudio. All rights reserved.
//

import UIKit
import SwiftyJSON;
import MGSwipeTableCell

class Notificacoes: GCViewController, UITableViewDelegate ,UITableViewDataSource {
    
    var dados                           :[DataNotificacoes] = [DataNotificacoes]();
    @IBOutlet weak var tableView: UITableView!
    let refreshControl = UIRefreshControl();

    static var INSTANCE:Notificacoes!;
    
    
      @IBAction func onClickClose(_ e:Any)
      {
          dismiss(animated: true, completion: nil);
      }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.register(UINib(nibName: "NotificacoesCell", bundle: nil), forCellReuseIdentifier: "NotificacoesCell")
        self.tableView.register(UINib(nibName: "NotificacoesCellData", bundle: nil), forCellReuseIdentifier: "NotificacoesCellData");
        
        tableView.rowHeight = UITableView.automaticDimension;
        tableView.estimatedRowHeight = 77.0;
        tableView.tableFooterView = UIView(frame: CGRect.zero);
        
        Notificacoes.INSTANCE = self;
        initUpdateRefresc();
        initModel();
        
        tableView.backgroundColor = .white
        view.backgroundColor = .white
        
    }
    
    //MARK: SWIPE
    
    
    func initUpdateRefresc(){
        
        let tableViewController = UITableViewController()
        tableViewController.tableView = tableView
        tableViewController.refreshControl = refreshControl;
        refreshControl.tintColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1);
        refreshControl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1);
        refreshControl.addTarget(self, action:#selector(onUpdateRefresc), for: UIControl.Event.valueChanged)
        
    }
    
    @objc func onUpdateRefresc()
    {
        initModel();
    }
    
    //MARK: TABLEVIEW
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return dados.count;
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension;
        
    }
    
    
    var entrou = false;
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if(dados[indexPath.row].isTitle)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "NotificacoesCellData") as! NotificacoesCellData;
            cell.set(value: dados[indexPath.row].titulo);
            cell.isUserInteractionEnabled = false;
            
            
            return cell;
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificacoesCell") as! NotificacoesCell;
        cell.initJson(json: dados[indexPath.row].json);
        
        let desfavoritar = MGSwipeButton(title: "", icon: UIImage(named:"ic_notificacoes_excluir_bg_red.png"), backgroundColor: .white) {
            (sender: MGSwipeTableCell!) -> Bool in
            
            cell.hideSwipe(animated: true)
            
            DispatchQueue.global(qos: .utility).async {
                
                DispatchQueue.main.async {
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                        
                        self.initModelRemove(id:self.dados[indexPath.row].json["id"].int!);
                    }
                    
                }
            }
            self.tableView.reloadData();
            
            return true
            
        }
        
        cell.rightButtons = [desfavoritar];
        
        if(!entrou)
        {
            
            cell.showSwipe(.rightToLeft, animated: true)
            
            DispatchQueue.global(qos: .utility).async {
                
                DispatchQueue.main.async {
                    
                    //self.tableView.setEditing(true, animated: true)
                    
                    let seconds = 2.0
                    DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
                        
                        cell.hideSwipe(animated: true)
                        
                    }
                    
                }
            }
            
            entrou = true;
        }
        
        cell.leftSwipeSettings.transition = .drag
        
        
        return cell;
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }
    
    //MARK: MODEL
    
    func initModel(){
        
        var p       = [String:Any]();
        p["token"] = DataUser.CURRENT.token;

        HTTP.request(URL:"\(HTTP.SERVER)user/notificacoes",params:p,isLoad: true, completion:{json,status in
            
            if(status == "")
            {
                
                if(json["data"] == JSON.null)
                {
                    return;
                }
                
                self.refreshControl.endRefreshing();

                self.onRender(array: json["data"].array!);
                
            }
            
        });
        
    }
    
    func initModelRemove(id:Int){
        
        var p       = [String:Any]();
        p["profile_id"] = "\(id)"
        p["id"] = "\(id)"
        p["token"] = DataUser.CURRENT.token;

        
        HTTP.request(URL:"\(HTTP.SERVER)user/notificacoes/delete",params:p,isLoad: true, completion:{json,status in
            
            if(status == "")
            {
                
                self.initModel();
                
            }
            
        });
        
    }
    
    
    func onRender(array:[JSON])
    {
        
        if(array.count <= 0)
        {
            return;
        }
        
        
        
        dados.removeAll();
        tableView.reloadData();
        
        let auxDados  = array;
        
        var dataAtual = array[0]["data"].string!;
        
        let dataData = DataNotificacoes();
        dataData.isTitle = true;
        dataData.titulo = dataAtual;
        self.dados.append(dataData);
        
        for json in auxDados
        {
            
            if(dataAtual != json["data"].string!)
            {
                
                dataAtual                = json["data"].string!;
                
                let dataDataNova         = DataNotificacoes();
                dataDataNova.isTitle     = true;
                dataDataNova.titulo      = json["titulo"].string!;
                
                self.dados.append(dataDataNova);
                
            }
            
            let data = DataNotificacoes();
            data.isTitle = false;
            data.json = json;
            self.dados.append(data);
            
        }
        
        if(self.dados.count <= 0)
        {
            showAviso(txt: "Não há notificações.")
        }else{
            hideAviso();
        }
    
        tableView.reloadData();
        
    }
    
}//END CLASS
