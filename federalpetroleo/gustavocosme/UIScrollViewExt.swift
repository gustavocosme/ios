//
//  UIScrollViewExt.swift
//  zoapet
//
//  Created by Gustavo Cosme on 20/03/20.
//  Copyright © 2020 SafariStudio. All rights reserved.
//

import UIKit

extension UIScrollView {
    var currentPage:Int{
        return Int((self.contentOffset.x+(0.5*self.frame.size.width))/self.frame.width)+1
    }
}
