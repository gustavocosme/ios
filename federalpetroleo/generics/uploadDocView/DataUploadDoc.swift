//
//  DataUploadDoc.swift
//  federalpetroleo
//
//  Created by Gustavo Cosme on 13/08/20.
//  Copyright © 2020 Safari. All rights reserved.
//

import UIKit

class DataUploadDoc:NSObject {

    var mask = "";
    var title = "";
    var isValidation = false;
    var isComplete = false;
    var txtDoc = "";
    var imageDoc = UIImage();
    var paramsName = "";
    var typeSend = "0";
    var placeHolder = "";
    var urlPatch = "";
    var imageUrl = "";
    var paramsTitle = "";


}

enum EnumUploadDoc {
    
    case AGUARDANDO
    case ERRO
    case OK
}
