//
//  DocumentosCredito.swift
//  federalpetroleo
//
//  Created by Gustavo Cosme on 24/08/20.
//  Copyright © 2020 Safari. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import SwiftyJSON

class DocumentosCredito: UIViewController,IndicatorInfoProvider {

    @IBOutlet weak var rgECPF: UploadDocViewButton!
    @IBOutlet weak var comprovanteDeResidencia: UploadDocViewButton!
    @IBOutlet weak var certidaoCadamento: UploadDocViewButton!
    @IBOutlet weak var rgECPFDosConjuge: UploadDocViewButton!
    @IBOutlet weak var procuracao: UploadDocViewButton!
    @IBOutlet weak var alvaraDeFuncionamento: UploadDocViewButton!
    @IBOutlet weak var certidaoDePropiedade: UploadDocViewButton!
    @IBOutlet weak var relacaoDeFaturamento: UploadDocViewButton!
    @IBOutlet weak var balancoPatrimonial: UploadDocViewButton!
    @IBOutlet weak var daclaracaoDeRecibo: UploadDocViewButton!
    @IBOutlet weak var fotosDoPostoQuadro: UploadDocViewButton!
    var array:[UploadDocViewButton] = [UploadDocViewButton]();
    var dados:[JSON] = [JSON]();

    static var INTANCE:DocumentosCredito!;
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
         
         return IndicatorInfo(title: "  Á Prazo  ")
         
     }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        DocumentosCredito.INTANCE = self;
            
        array.append(rgECPF);
        array.append(comprovanteDeResidencia);
        array.append(certidaoCadamento);
        array.append(rgECPFDosConjuge);
        array.append(procuracao);
        array.append(alvaraDeFuncionamento);
        array.append(certidaoDePropiedade);
        array.append(relacaoDeFaturamento);
        array.append(balancoPatrimonial);
        array.append(daclaracaoDeRecibo);
        array.append(fotosDoPostoQuadro);
        
        initModel();
    }
    
    
    
    func initModel()
    {
        
        var p:[String:Any] = [String:Any]();
        p["token"] = DataUser.CURRENT.token;
        p["posto_id"] = String(DataUser.CURRENT.id)

        HTTP.request(URL:"\(HTTP.SERVER)user/doc/a-prazo",params:p,isLoad:true,completion:{json,status in
        
            if(json["status"].bool!)
            {
                self.dados = json["data"].array!;
                self.onRender();
            }
            else{
                self.addAlertOK(value: "Inválido!")
            }
         
        });
        
        
        
    }
    
    func onRender()
    {
        var p = 0;
        
        if(p < 11)
        {
            for i in array {
                
                if(dados[p]["status"].string! != "")
                {
                    if(dados[p]["status"].string! == "aguardando")
                    {
                        i.setStatus(status: .AGUARDANDO);
                    }
                    else
                    if(dados[p]["status"].string! == "liberado")
                    {
                        
                        i.datas.imageUrl = dados[p]["arquivo"].string!;
                        i.setStatus(status: .OK);
                        
                    }
                    else
                    {
                        
                        i.setStatus(status: .ERRO);
                        
                    }
                }
           

            }
        
            p += 1;
            
        }
        
    }

}//END CLASS

