//
//  DataGC.swift
//  PEnoCarnaval
//
//  Created by Gustavo Cosme on 12/11/18.
//  Copyright © 2018 SafariStudio. All rights reserved.
//

import UIKit

extension Date {
    /**
     Formats a Date
     
     - parameters format: (String) for eg dd-MM-yyyy hh-mm-ss
     */
    
    func asString(style: DateFormatter.Style) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = style
        return dateFormatter.string(from: self)
    }
    
    func formatDateGakeria(format:String = "dd-MM-yyyy") -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        let dateString = dateFormatter.string(from: self)
        if let newDate = dateFormatter.date(from: dateString) {
            //print(dateString)
            return newDate
        } else {
            //print("ERRO")
            
            return self
        }
    }
    
    func formatDate(format:String = "yyyy-MM-dd") -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        let dateString = dateFormatter.string(from: self)
        if let newDate = dateFormatter.date(from: dateString) {
            //print(dateString)
            return newDate
        } else {
            //print("ERRO")

            return self
        }
    }
}
