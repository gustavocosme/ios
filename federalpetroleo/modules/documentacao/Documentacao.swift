//
//  Pets.swift
//  zoapet
//
//  Created by Gustavo Cosme on 05/02/19.
//  Copyright © 2019 SafariStudio. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import Kingfisher;
import SwiftyJSON;

class Documentacao: ButtonBarPagerTabStripViewController {

    
    //MARK: SET
    
    @IBAction func onClickClose(_ e:Any)
    {
        dismiss(animated: true, completion: nil);
    }
    
    override func viewDidLoad() {
        super.viewDidLoad();
                
        buttonBarView.selectedBar.backgroundColor = #colorLiteral(red: 0.1311326921, green: 0.190726459, blue: 0.5117009282, alpha: 1)
        buttonBarView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1);
        
        settings.style.buttonBarItemFont = .boldSystemFont(ofSize: 14)
        settings.style.buttonBarItemsShouldFillAvailableWidth = true
        settings.style.buttonBarLeftContentInset = 0
        settings.style.buttonBarRightContentInset = 0
        settings.style.selectedBarHeight = 0.3;
        settings.style.buttonBarBackgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1);
        settings.style.buttonBarItemTitleColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1);
        settings.style.buttonBarItemBackgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1);
        settings.style.selectedBarBackgroundColor = #colorLiteral(red: 0.1300109923, green: 0.1890083551, blue: 0.5110445619, alpha: 1);
        
        
    }
    

    //MARK: PAGES
    
    
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        
        let child_1 = AppDelegate.STAGE.getDocumentosCredito()
        let child_2 = AppDelegate.STAGE.getDocumentosDebito();
        return [child_1,child_2];
        
    }
    
    
    
    
    
   
    
    
    
}//END CLASS
