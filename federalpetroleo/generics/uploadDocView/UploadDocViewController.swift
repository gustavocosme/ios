//
//  UploadDocViewController.swift
//  federalpetroleo
//
//  Created by Gustavo Cosme on 13/08/20.
//  Copyright © 2020 Safari. All rights reserved.
//

import UIKit
import JMMaskTextField_Swift;
import PKHUD
import Alamofire
import SwiftyJSON;
import Kingfisher

class UploadDocViewController: UIViewController, UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    var datas:UploadDocViewButton!;
    
    @IBOutlet weak var txt             : JMMaskTextField!
    @IBOutlet weak var image           : UIImageView!
    @IBOutlet weak var titleView       : UILabel!
    
    let imagePicker  = UIImagePickerController()
    var dataImageUIImage1               :UIImage!;
    
    
    override func viewDidLoad() {
        super.viewDidLoad();
        
        imagePicker.delegate = self;
        
        titleView.text = datas.datas.title;
        txt.placeholder = datas.datas.placeHolder;
        
        
        if(datas.datas.mask != "")
        {
            txt.maskString = datas.datas.mask;
        }
        
        if(datas.datas.imageUrl != "")
        {
            let url = URL(string:datas.datas.imageUrl)
            image.kf.setImage(with: url);
            
        }
        
        image.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(UploadDocViewController.onPhoto))
        image.addGestureRecognizer(tap)
        
    }
    
    @IBAction func onClick(sender: Any) {
        
        if(txt.text!.count == 0)
        {
            addAlertOK(value: "Informe o documento");
            return;
        }
        
        if(dataImageUIImage1 == nil)
        {
            addAlertOK(value: "Adicione a imagem");
            return;
        }
        
        if(datas.datas.typeSend == "1")
        {
            initModel();
            return;
        }
        
        self.datas.datas.isComplete = true;
        self.datas.datas.txtDoc = self.txt.text!;
        self.datas.datas.imageDoc = self.dataImageUIImage1;
        //self.datas.txt.text = self.txt.text!;
        self.datas.setStatus(status: .OK);
        
        dismiss(animated: true, completion: nil);
        
    }
    
    func initModel()
    {
        
        let i1 = DataUploadImage();
        i1.nameParams = datas.datas.paramsName;
        i1.image = dataImageUIImage1
              
        var p:[String:Any] = [String:Any]();
        p["id"] = String(DataUser.CURRENT.id);
        p["posto_id"] = String(DataUser.CURRENT.id)

        p["token"] = DataUser.CURRENT.token;
        p[datas.datas.paramsTitle] = String(txt.text!);

        UploadImage().upload("\(HTTP.SERVER)\(datas.datas.urlPatch)", images:[i1], params: p, completion: {json in
                  
                  DispatchQueue.global(qos: .utility).async {
                         
                             DispatchQueue.main.async {
                              
                                      //print(json.description);
                                
                                      let actionSheetController: UIAlertController = UIAlertController(title:nil, message: "Documento enviado com sucesso!", preferredStyle: .alert)
                                       
                                  
                                       
                                       let okAction: UIAlertAction = UIAlertAction(title: "Ok", style: .default) { action -> Void in
                                           
                                             //self.datas.setStatus(status: .AGUARDANDO);
                                        
                                            if(DocumentosCredito.INTANCE != nil)
                                            {
                                                DocumentosCredito.INTANCE.initModel();
                                            }
                                            
                                            if(DocumentosDebito.INTANCE != nil)
                                            {
                                                DocumentosDebito.INTANCE.initModel();
                                            }
                                                                                        
                                            self.dismiss(animated: true, completion: nil);
                                            
                                       }
                                       
                                       actionSheetController.addAction(okAction)
                                       self.present(actionSheetController, animated: true, completion: nil)
                                      
                                   
                                      
                              
                             }
                         }
                 

              })
              
        
        
        
    }
   
    
    
    @IBAction func onClickClose(sender: Any) {
        
        dismiss(animated: true, completion: nil);
        
    }
    
    
    //###################################################//
    //MARK:PHOTO
    //###################################################//
    
    @IBAction func onClickDelete(sender: Any) {
        
        image.image = nil;
        
    }
    
    
    @objc func onPhoto() {
        
        
        
        
        let refreshAlert = UIAlertController(title: "Foto perfil", message: "Modo de carregamento.", preferredStyle:.alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Biblioteca", style: .default, handler: { (action: UIAlertAction!) in
            
            self.imagePicker.allowsEditing = false
            self.imagePicker.sourceType = .photoLibrary
            self.present(self.imagePicker, animated: true, completion: nil)
            
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "Foto", style: .default, handler: { (action: UIAlertAction!) in
            
            if !UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
                
                return
            }
            
            self.imagePicker.allowsEditing = false;
            self.imagePicker.sourceType = UIImagePickerController.SourceType.camera
            self.imagePicker.cameraCaptureMode = .photo
            
            //self.imagePicker.modalPresentationStyle = .fullScreen
            self.present(self.imagePicker,
                         animated: true,
                         completion: nil)
            
            
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: { (action: UIAlertAction!) in
            
            
        }))
        
        present(refreshAlert, animated: true, completion: nil);
        
        
        
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        
        
        var newImage: UIImage
        
        if let possibleImage = info[.editedImage] as? UIImage {
            
            newImage = possibleImage
            self.dataImageUIImage1 = newImage.normalizedImage();
            self.image.image = dataImageUIImage1
            
        } else if let possibleImage = info[.originalImage] as? UIImage {
            
            newImage = possibleImage
            self.dataImageUIImage1 = newImage.normalizedImage();
            
            self.image.image = dataImageUIImage1
            
        } else {
            return
        }
        
        
        dismiss(animated: true)
    }
    
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
        
    }
    
    
    
    
    
    
    
    
    
    
}//END CLASS
