//
//  AppDelegate.swift
//  federalpetroleo
//
//  Created by Gustavo Cosme on 04/06/20.
//  Copyright © 2020 Safari. All rights reserved.
//

import UIKit
import CoreData
import IQKeyboardManagerSwift;
import OneSignal
import NotificationCenter


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    static var STAGE            :Stage = Stage();
    static var  APP             :AppDelegate!
    static var  IS_TESTE               = false
    static var USER:DataUser           = DataUser();
    static var TOKEN_PUSH = "";

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        if #available(iOS 13.0, *) {
             
            window?.overrideUserInterfaceStyle = .light
                 
        }
        
        
        
        AppDelegate.APP = self;
        IQKeyboardManager.shared.enable = true;
        
        let settings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
        application.registerUserNotificationSettings(settings)
        application.registerForRemoteNotifications()
        application.applicationIconBadgeNumber = 0;

        let onesignalInitSettings = [kOSSettingsKeyAutoPrompt: false]

        OneSignal.initWithLaunchOptions(launchOptions,
                                    appId:"56f5c64a-2ee3-420e-ad58-25ffb4e67247",
                                            handleNotificationAction: nil,
                                            settings: onesignalInitSettings)
            
        OneSignal.inFocusDisplayType = OSNotificationDisplayType.notification;
         
        OneSignal.promptForPushNotifications(userResponse: { accepted in
                
                print("User accepted notifications: \(accepted)");
                //AppDelegate.TOKEN_PUSH = String(userId ?? "");
                //print("TOKEN PUSH",AppDelegate.TOKEN_PUSH);
                
        })
        
        
        DataUser.inicialize();
        
        if(UserDefaults.standard.object(forKey:"logado") == nil)
        {
               
            self.window?.rootViewController         = AppDelegate.STAGE.getNavAutenticacao();
            
        }else{
               
            print(DataUser.CURRENT.token);
            self.window?.rootViewController         = AppDelegate.STAGE.getNavMain();
            
        }
 
 

        return true

    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
       
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {

    }
    
    

}//END CLASS

extension UIApplication {

class func getTopViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {

    if let nav = base as? UINavigationController {
        return getTopViewController(base: nav.visibleViewController)

    } else if let tab = base as? UITabBarController, let selected = tab.selectedViewController {
        return getTopViewController(base: selected)

    } else if let presented = base?.presentedViewController {
        return getTopViewController(base: presented)
    }
    return base
}

}
